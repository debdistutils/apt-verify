# apt-verify README

Apt-verify extends [apt](https://salsa.debian.org/apt-team/apt) to
call all tools in `/etc/apt/verify.d/` instead of always only calling
`gpgv`, to verify apt archive integrity and authenticity.  A symbolic
link `/etc/apt/verify.d/gpgv` is installed to provide full backwards
compatibility by pointing towards `/usr/bin/gpgv`.

Apt-verify works through a small script
[apt-verify](apt-verify) that is small and easy to audit.
See the [apt-verify(8)](apt-verify.8.md) man page for
detailed documentation.

The motivation behind this project is to enable plugins such as
[apt-canary](https://gitlab.com/debdistutils/apt-canary) and
[apt-sigstore](https://gitlab.com/debdistutils/apt-sigstore), and pave
the road for other experiments.

[TOC]

## Package Installation

The recommended way is to install the `apt-verify` package, available
from the [apt-verify release
page](https://gitlab.com/debdistutils/apt-verify/-/releases).

```
wget -nv https://gitlab.com/debdistutils/apt-verify/uploads/5bf93e0b8a02193e64f23561b05a2ef4/apt-verify_2.0-1_all.deb
dpkg -i apt-verify*.deb
```

The packaging is maintained on the `debian` branch of the `apt-verify`
repository, see [apt-verify's debian
branch](https://gitlab.com/debdistutils/apt-verify/-/tree/debian?ref_type=heads).

## Manual Installation

Put the [apt-verify](apt-verify) script in the PATH used by
apt, typically `/usr/bin` or `/usr/local/bin`, and make sure it is
executable.

The script works with any POSIX-compliant /bin/sh such as dash or GNU
bash, and uses only common tools such as `find`, `sort` and `logger`.
Naturally, `apt` and `gpgv` needs to be installed as well.  On all
reasonable installation the requirements should already be installed,
please let us know if there is a way to reduce the dependencies
further.

Create the directory `/etc/apt/verify.d` and put a symlink to
`/usr/bin/gpgv` in it. See [verify.d/README](verify.d/README) for full
documentation on how the directory is used.

Don't worry if you fail to setup the `gpgv` symlink properly: `apt`
will complain loudly if it cannot find the expected GnuPG verification
output messages when it attempts to verify release files (or you have
hit a serious security bug in apt).

Configure `apt` to use the script as a gpgv drop-in, by setting
`APT::Key::gpgvcommand` to `"apt-verify"` in a
`/etc/apt/apt.conf.d/` file, we suggest using the filename `75verify`.

Essentially, the following commands installs the package.

```
wget -nv -O/usr/local/bin/apt-verify https://gitlab.com/debdistutils/apt-verify/-/raw/main/apt-verify
chmod +x /usr/local/bin/apt-verify
mkdir -p /etc/apt/verify.d
ln -s /usr/bin/gpgv /etc/apt/verify.d
echo 'APT::Key::gpgvcommand "apt-verify";' > /etc/apt/apt.conf.d/75verify
```

## Testing

Test it by running `apt-get update` and you should see something like
the following in `/var/log/syslog`:

```
Feb  8 12:32:08 kaka apt-verify: /etc/apt/verify.d/gpgv --homedir /tmp/apt-key-gpghome.i8sOBsTZA5 --keyring /tmp/apt-key-gpghome.i8sOBsTZA5/pubring.gpg --ignore-time-conflict --status-fd 3 /tmp/apt.sig.35yprB /tmp/apt.data.9zW8zG
```

The output reflects how `apt` called out to `apt-key verify` which
called out to `apt-verify`.  These are debug messages, so you may
wish to filter them out from the syslog and they may be disabled by
this project eventually.

## Status

I have tested apt-verify on the following systems:

- Trisquel GNU/Linux 11.0 aramo amd64 on NovaCustom NV41 laptop
- Trisquel GNU/Linux 10.0.0 Nabia amd64 on Dell R630 server
- Debian 11 (bullseye) on Raptor Computing Systems Talos II ppc64el server
- Debian 11 (bullseye) on amd64/ppc64el
- Debian 10 (buster) on amd64
- Ubuntu 20.04 focal on amd64
- Ubuntu 22.04 jammy on amd64
- Devuan GNU+Linux Chimaera 4.0 on amd64
- Gnuinos GNU/Linux Chimaera 4.0 on amd64

## Design Considerations

Replacing the `gpgv` tool is a a hack.  The functionality may be
integrated into apt natively, or via some other extension mechanism.

The advantages with our approach is that it is minimal, it is easy to
audit our code, does not require any changes to apt, and appears to
work fine cross-platform on many existing apt-based distributions.

One disadvantage with our approach is that it is currently not known
how to find out which apt repository (apt URL) was used to retrieve a
particular release file under verification.

## License

Any file in this project is free software: you can redistribute it
and/or modify it under the terms of the GNU Affero General Public
License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

See the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html] for license text.

## Contact

Please test and [send a
report](https://gitlab.com/debdistutils/apt-verify/-/issues) if
something doesn't work.

The author of this project is [Simon
Josefsson](https://blog.josefsson.org/).
